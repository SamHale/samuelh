#!usr/bin/python3
#This scritp will take inputs of firstname, lastname, and account type and create
#a user on both lab devices.
#
#Samuel-20210608: Initial version
#
#
import salt.client
WIN = 'Win2019'
LINUX = 'ip-172-31-48-109.ec2.internal'

#main function
def main():
    print('Enter "end" when all desired input has been entered')
    usernames = {}
    client = salt.client.LocalClient()
    while True:
        username = ''
        temp = input('Enter firstname: ')
        userinfo = []
        if temp == 'end':
            break
        else:
            userinfo.append(temp)
        temp = input('Enter lastname: ')
        if temp == 'end':
            break
        else:
            userinfo.append(temp)
        temp = input('Enter account type (student, staff): ')
        if temp == 'end':
            break
        elif temp == 'student' or temp == 'staff':
            userinfo.append(temp)
        else:
            while True:
                print('Account type must be exactly student or staff')
                temp = input('Enter account type: ')
                if temp == 'student' or temp == 'staff':
                    userinfo.append(temp)
                    break
        if temp == 'student':
            if len(userinfo[1]) <= 5:
                username += userinfo[1]
            else:
                username += userinfo[1][:5]
            if len(userinfo[0]) <= 3:
                username += userinfo[0]
            else:
                username += userinfo[0][:3]
            username = username.lower()
            try:
                userinfo.append(username + str(usernames[username]).zfill(2))
                usernames[username] += 1
            except:
                userinfo.append(username + '00')
                usernames[username] = 1
        else:
            username = userinfo[0][0] + userinfo[1]
            username = username.lower()
            try:
                userinfo.append(username + str(usernames[username]))
                usernames[username] += 1
            except:
                userinfo.append(username)
                usernames[username] = 2
        client.cmd(LINUX, 'user.add', [username, 'groups='+temp])
        client.cmd(LINUX, 'shadow.set_password', [username, '$6$s0zClapRuJww/Wsr$WVXEJ/yIsMTyPGMOb8JnC7Bb71vKnqOdL1Srs4BAgfRyiDmIpYJN5wqRKJje8kAhmCbQReneolId4ZoEBbGoQ1'])
        client.cmd(WIN, 'user.add', [username, 'Pa$$w0rd', 'groups="Remote Desktop Users"'])
        client.cmd(WIN, 'user.addgroup', [username, temp])



if __name__ == '__main__':
    main()
