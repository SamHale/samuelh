#!/usr/bin/python3
#this program uses salt to retrieve information about managed systems and store it 
#in a database. must be run as the user that has access to salt (sudo) or add
#external authentication to both salt master and this program.
#
#Samuel-20210606: Initial version
import salt.client
import salt.runner
import pymysql
SERVER = 'localhost'
USER ='root'
PASSWD = 'Pa$$w0rd'
DB = 'cmdb'
TABLE = 'device'


#main function
def main():
    db = pymysql.connect(host=SERVER, user=USER, password=PASSWD, database=DB)
    cursor = db.cursor()
    caller = salt.client.Caller
    client = salt.client.LocalClient()
    clientrun = salt.runner.RunnerClient
    ret = client.cmd('*', 'grains.item', ['fqdn'])
    output = []
    tmp = []
    for key, value in ret.items():
        tmp.append(value['fqdn'])
    output.append(tmp)
    tmp = []
    ret = client.cmd('*', 'grains.item', ['hwaddr_interfaces'])
    for key, value in ret.items():
        #print(value)
        for key2, value2 in value.items():
            for key3, value3 in value2.items():
                if not (value3 == '00:00:00:00:00:00' or value3 == ':::::'):
                    tmp.append(value3)
    output.append(tmp)
    tmp = []
    ret = client.cmd('*', 'network.ip_addrs')
    for key, value in ret.items():
        tmp.append(value[0])
    output.append(tmp)
    tmp = []
    ret = client.cmd('*', 'grains.item', ['num_cpus'])
    for key, value in ret.items():
        for key2, value2 in value.items():
            tmp.append(value2)
    output.append(tmp)
    tmp = []
    ret = client.cmd('*', 'ps.disk_partitions')
    for key, value in ret.items():
        tmp.append(len(value))
    output.append(tmp)
    tmp = []
    ret = client.cmd('*', 'ps.total_physical_memory')
    for key, value in ret.items():
        tmp.append(int(round(value/1073741824, 0)))
    output.append(tmp)
    tmp = []
    ret = client.cmd('*' , 'grains.item', ['os'])
    for key, value in ret.items():
        tmp.append(value['os'])
    output.append(tmp)
    tmp = []
    ret = client.cmd('*', 'grains.item', ['osrelease'])
    for key, value in ret.items():
        tmp.append(value['osrelease'])
    output.append(tmp)
    tmp = []
    corrected = []
    for count in range(len(output[0])):
        for collum in output:
            tmp.append(collum[count])
        cursor.execute('insert into device(name, macaddress, IP, cpucount, disks, ram, ostype, osversion) values '+str(tuple(tmp)))
        tmp = []
    db.commit()



if __name__ == '__main__':
    main()
