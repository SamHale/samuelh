#!/usr/bin/python3
#Averages three user inputed numbers to two decimal places
#
#Samuelh-20210503 Initial version
#
#
import sys
#Main routine
def main():
    args = sys.argv[1:]
    try:
        int(args[0])
        int(args[1])
        int(args[2])
    except:
        usage()
    else:
        try:
            args[3]
        except:
            avg = (int(args[0]) + int(args[1]) + int(args[2])) / 3
            print('Average of ', args[0], ', ', args[1], ', and ', args[2], ' is ',
                    str(round(avg, 2)))
        else:
            usage()
    

#function for printing out usage
def usage():
    print("This program accepts input as three numbers seperated by spaces(3 8 11).")


#Run the main routine if sript called directly
if __name__ == "__main__":
    main()
