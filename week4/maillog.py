#!/usr/bin/python3
#This program will accept a log file and create a csv file with the name and ip
#of individual servers that sent eamil.
#
#Samuel-20210504: Initial version
#
#
import csv
import re

#Main routine
def main():
    with open('servers.csv', 'w', newline='') as fout:
        csvout = csv.writer(fout)
        csvout.writerow(['Server Name', ' IP Address']) 
        dedupmatches = []
        with open(input('Enter the name of file to serach through: '), 'r') as fin:
            matches = re.finditer(r".* connect.* (.*)\[(.*)\].*", fin.read())
            for match in matches:
                if match.groups() not in dedupmatches:
                    dedupmatches.append(match.groups())
        for servers in dedupmatches:
            csvout.writerow([servers[0], ' ' + servers[1]])

#run main() if script called directly
if __name__ == '__main__':
    main()
