#!usr/bin/python3
#This script will read dta from an sql database and write it to a csv or json file
#
#Samuel-20210525: Initial verzion
#
import pymysql
import sys
import csv
import json
SERVER = 'localhost'
USER = 'root'
PASSWD = 'Pa$$w0rd'
DB = 'cmdb'
TABLE = 'device'

#Main function
def main():
    args = sys.argv[1]
    db = pymysql.connect(host=SERVER, user=USER, password= PASSWD, database=DB)
    cursor = db.cursor()
    cursor.execute('DESC '+TABLE)
    columnnames = [column[0] for column in cursor.fetchall()] 
    cursor.execute('SELECT * FROM '+TABLE)
    data = cursor.fetchall()
    if args == 'csv':
        with open ('database.csv', 'w', newline='') as fout:
            csvout = csv.writer(fout)
            csvout.writerow(columnnames)
            for row in data:
                csvout.writerow(row)
    elif args == 'json':
        datadict = {}
        for name in columnnames:
            datadict[name] = []
        for row in data:
            tmp = 0
            for column in datadict:
                datadict[column].append(row[tmp])
                tmp += 1
        with open ('database.json', 'w') as fout:
                json.dump(datadict, fout)
    else:
        print('Acceptable arguments are csv or json')
    



if __name__ == '__main__':
    main()
