#!usr/bin/python
#This program will input some system information into a mysql database.
#
#Samuel-20210525: Initial version
#
import platform
import psutil
import pymysql
SERVER = 'localhost'
USER ='root'
PASSWD = 'Pa$$w0rd'
DB = 'cmdb'
TABLE = 'device'

#main function
def main():
    db = pymysql.connect(host=SERVER, user=USER, password= PASSWD, database=DB)
    cursor = db.cursor()
    system = platform.uname()
    sysver = system.version
    if len(sysver) > 20:
        sysver = sysver[:20]
    netints = psutil.net_if_addrs().items()
    netifs = []
    for interface in netints:
        netifs.append(interface)
    values = (str(system.node), netifs[1][1][2].address, netifs[1][1][0].address, psutil.cpu_count(), len(psutil.disk_partitions()), int(round(psutil.virtual_memory().total/1073741824, 0)), system.system, sysver)

    cursor.execute('insert into device(name, macaddress, IP, cpucount, disks, ram, ostype, osversion) values '+str(values))
    db.commit()


if __name__ == '__main__':
    main()
