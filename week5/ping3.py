#!usr/bin/python3
#This program accepts either a file containing names/IPs or an IP/name
#then pings the destination(S) and prnts the results
#
#Samuel-20210510: Initial version
#
#
import ping1
import ping2
import sys

#Main function
def main():
    args = sys.argv[1:]
    try:
        try:
            with open(args[0], 'r') as test:
                ping1.ping(args[0])
        except:
            ping2.ping(args[0])
    except:
        print('This program accepts an argument in the form of an IP/domain name or the name of a file containing a list of IPs/domain names.')


if __name__ == '__main__':
    main()
