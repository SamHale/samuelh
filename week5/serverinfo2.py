#!usr/bin/python3
#This prgram creates a list of system infromation in a user defined format
#csv, json, screen
#
#Samuel-20210511: Initial version
#
import psutil
import platform
import serverinfo1
import sys
import csv
import json

#main function
def main():
    args = sys.argv[1:]
    if args[0] == 'screen':
        serverinfo1.main()
    elif args[0] == 'csv':
        csvoutput()
    elif args[0] == 'json':
        jsonout()
    else:
        print('Acceptable format types are csv, json, or screen')


#Function to create output in csv format
def csvoutput():
    with open ('serverinfo.csv', 'w', newline='') as fout:
        csvout = csv.writer(fout)
        csvout.writerow(['hostname', 'CPU (count)', 'RAM (GB)', 'OSType', 'OSVersion', 'Disks (count)', 'NIC (count)', 'NIC names/IPs'])
        system = platform.uname()
        values = []
        values.append(system.node)
        values.append(psutil.cpu_count())
        values.append(int(round(psutil.virtual_memory().total/1073741824, 0)))
        values.append(system.system)
        values.append(system.version)
        values.append(len(psutil.disk_partitions()))
        netints = psutil.net_if_addrs().items()
        values.append(len(netints))
        for interface, addrs in netints:
            addrout = ''
            for addr in addrs:
                if str(addr.family) == 'AddressFamily.AF_INET':
                    addrout += addr.address + ' '
            values.append(interface + ': ' + addrout)
        csvout.writerow(values)

#Function to create output in json format
def jsonout():
    system = platform.uname()
    info = {}
    info['hostname'] = system.node
    info['CPU (count)'] = psutil.cpu_count()
    info['RAM (GB)'] = int(round(psutil.virtual_memory().total/1073741824, 0))
    info['OSType'] = system.system
    info['OSVersion'] = system.version
    info['Disks (count)'] = len(psutil.disk_partitions())
    netints = psutil.net_if_addrs().items()
    info['NIC (count)'] = len(netints)
    #fout.write('NIC names/IPs:\n')
    for interface, addrs in netints:
        addrout = []
        for addr in addrs:
            if str(addr.family) == 'AddressFamily.AF_INET':
                addrout.append(addr.address)
        info[interface] = addrout
    with open ('serverinfo.json', 'w') as fout:
        json.dump(info, fout)


if __name__ == '__main__':
    main()
