#!usr/bin/python
#This program will print out information about the system that it is executed on.
#
#Samuel-20210511: Initial version
#
import platform
import psutil


#main function
def main():
    system = platform.uname()
    print('hostname =', system.node)
    print('CPU (count) =', psutil.cpu_count())
    print('RAM (GB) =', int(round(psutil.virtual_memory().total/1073741824, 0)))
    print('OSType =', system.system)
    print('OSVersion =', system.version)
    print('Disks (count) =', len(psutil.disk_partitions()))
    netints = psutil.net_if_addrs().items()
    print('NIC (count) =', len(netints))
    print('NIC names/IPs:')
    for interface, addrs in netints:
        print('  ' + interface, end=(' = '))
        for addr in addrs:            
            if str(addr.family) == 'AddressFamily.AF_INET':
                print(addr.address, end=' ')
        print()




if __name__ == '__main__':
    main()
