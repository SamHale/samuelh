#!usr/bin/python3
#This script will accept a file containing a list of sites and IP addresses to ping.
#The program will then list what the target was and the time or unavailable for the
#ping to be reterned.
#
#Samuel-20210509: Initial version
#
import os
import sys
import re

#main function
def main():
    args = sys.argv[1:]
    try:
        ping(args[0])
    except:
        print('This program accepts one argument in the form of a filename.')


#accept filename ping contents and print out results
def ping(filename):
    print('IP,\t TimeToPing(ms)')
    with open(filename, 'r') as fin:
        for line in fin.readlines():
            ping = os.system('ping -c1 ' + line.rstrip() + ' > pingres')
            print(line.rstrip(), end=', ')
            if ping == 0:
                with open('pingres', 'r') as results:
                    time = re.search(r'rtt.*\/(.*)\/.*\/', results.read())
                    print(time[1])
            else:
                print('Not Found')
    os.system('rm pingres')
     


if __name__ =='__main__':
    main()
