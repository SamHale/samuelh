#!usr/bin/python
#This program will take input of a domain name or IP address, attempt to ping
#that destination, and print out the results
#
#Samuel-20210510: Initial version
#
import os
import sys
import re

#amin function
def main():
    args = sys.argv[1:]
    try:
        ping(args[0])
    except:
        print('This program accepts one argument in the form of an IP or domain name.')

#accept IP/name pings and prints out results
def ping(dest):
    print('IP,\t TimeToPing(ms)')
    ping = os.system('ping -c1 ' + dest + ' > pingres')
    print(dest, end=', ')
    if ping == 0:
        with open('pingres', 'r') as results:
            time = re.search(r'rtt.*\/(.*)\/.*\/', results.read())
            print(time[1])
    else:
        print('Not Found')
    os.system('rm pingres')

if __name__ =='__main__':                                                                  main()                   
