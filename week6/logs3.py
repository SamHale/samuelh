#!/usr/bin/python3
#This program will take input as a file listing IP addresses and create a csv file 
#containing the IP, Mac address, and the vendor that produced that device.
#
#Samuel-20210517: Initial version
#
import re
import csv
import requests
import time

#main function
def main():
    with open (input('Enter the name of the file containing IPs to search: '), 'r') as fin:
        log = input('Enter the name of the log file: ')
        with open ('log3.csv', 'w', newline='') as fout:
            csvout = csv.writer(fout)
            csvout.writerow(['IP Address', 'Mac Address', 'Vendor'])
            output = []
            for line in fin:
                regex = 'for (' + line.rstrip() + ').* from (.................)'
                with open (log, 'r') as logf:
                    matches = re.finditer(regex, logf.read())
                uniqueids = []
                for match in matches:
                    if match.groups()[0] + match.groups()[1] not in uniqueids:
                        uniqueids = match.groups()[0] + match.groups()[1]
                        temp = [match.groups()[0], match.groups()[1]]
                        time.sleep(1)
                        r = requests.get(url="http://api.macvendors.com/%s" %match.groups()[1])
                        if r.text:
                            temp.append(r.text)
                        else:
                            temp.append('Not found')
                        output.append(temp)
            for entry in output:
                csvout.writerow(entry)






if __name__ == '__main__':
    main()
