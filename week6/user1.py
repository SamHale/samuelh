#!usr/bin/python3
#This scritp will take inputs of firstname, lastname, school, and account type.
#This information will then be stored in  csv file.
#
#Samuel-20210571: Initial version
#
#
import csv

#main function
def main():
    print('Enter "end" when all desired input has been entered')
    with open ('user1.csv', 'w', newline='') as fout:
        csvout = csv.writer(fout)
        csvout.writerow(['Fistname', 'Lastname', 'School', 'Accounttype'])
        while True:
            temp = input('Enter firstname: ')
            userinfo = []
            if temp == 'end':
                break
            else:
                userinfo.append(temp)
            temp = input('Enter lastname: ')
            if temp == 'end':
                break
            else:
                userinfo.append(temp)
            temp = input('Enter school abreviation (IHS, NJH, AH, BHS, or BC): ')
            if temp == 'end':
                break
            elif temp == 'IHS' or temp == 'NJH' or temp == 'AH' or temp == 'BHS' or temp == 'BC':
                userinfo.append(temp)
            else:
                while True:
                    print('The school abreviation must be IHS, NJH, AH, BHS, or BC')
                    temp = input('Enter school abreviation: ')
                    if temp == 'IHS' or temp == 'NJH' or temp == 'AH' or temp == 'BHS' or temp == 'BC':
                        userinfo.append(temp)
                        break
            temp = input('Enter account type (student, staff): ')
            if temp == 'end':
                break
            elif temp == 'student' or temp == 'staff':
                userinfo.append(temp)
            else:
                while True:
                    print('Account type must be exactly student or staff')
                    temp = input('Enter account type: ')
                    if temp == 'student' or temp == 'staff':
                        userinfo.append(temp)
                        break
            csvout.writerow(userinfo)




if __name__ == '__main__':
    main()
