#!usr/bin/python3
#This scritp will take inputs of firstname, lastname, school, and account type.
#This information will then be stored in csv file allong with a gernerated username.
#
#Samuel-20210571: Initial version
#
#
import csv

#main function
def main():
    print('Enter "end" when all desired input has been entered')
    usernames = {}
    with open ('user2.csv', 'w', newline='') as fout:
        csvout = csv.writer(fout)
        csvout.writerow(['Fistname', 'Lastname', 'School', 'Accounttype', 'Username'])
        while True:
            temp = input('Enter firstname: ')
            userinfo = []
            if temp == 'end':
                break
            else:
                userinfo.append(temp)
            temp = input('Enter lastname: ')
            if temp == 'end':
                break
            else:
                userinfo.append(temp)
            temp = input('Enter school abreviation (IHS, NJH, AH, BHS, or BC): ')
            if temp == 'end':
                break
            elif temp == 'IHS' or temp == 'NJH' or temp == 'AH' or temp == 'BHS' or temp == 'BC':
                userinfo.append(temp)
            else:
                while True:
                    print('The school abreviation must be IHS, NJH, AH, BHS, or BC')
                    temp = input('Enter school abreviation: ')
                    if temp == 'IHS' or temp == 'NJH' or temp == 'AH' or temp == 'BHS' or temp == 'BC':
                        userinfo.append(temp)
                        break
            temp = input('Enter account type (student, staff): ')
            if temp == 'end':
                break
            elif temp == 'student' or temp == 'staff':
                userinfo.append(temp)
            else:
                while True:
                    print('Account type must be exactly student or staff')
                    temp = input('Enter account type: ')
                    if temp == 'student' or temp == 'staff':
                        userinfo.append(temp)
                        break
            if temp == 'student':
                username = ''
                if len(userinfo[1]) <= 5:
                    username += userinfo[1]
                else:
                    username += userinfo[1][:5]
                if len(userinfo[0]) <= 3:
                    username += userinfo[0]
                else:
                    username += userinfo[0][:3]
                username = username.lower()
                try:
                    userinfo.append(username + str(usernames[username]).zfill(2))
                    usernames[username] += 1
                except:
                    userinfo.append(username + '00')
                    usernames[username] = 1
            else:
                username = userinfo[0][0] + userinfo[1]
                username = username.lower()
                try:
                    userinfo.append(username + str(usernames[username]))
                    usernames[username] += 1
                except:
                    userinfo.append(username)
                    usernames[username] = 2
            csvout.writerow(userinfo)




if __name__ == '__main__':
    main()
