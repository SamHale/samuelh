#!/usr/bin/python3
#This script will take a log file and create a csv file containing unique IP and MAC
#addresses and the number of DHCP ACK requests sent by each
#
#Samuel-20210517:Initial version
#
import re
import csv

#main function
def main():
    with open(input('Enter the log file to search through: '), 'r') as log:
        matches = re.finditer(r'ACK on (.*) to (.................)', log.read())
    devices = {}
    for match in matches:
        ipmac = match.groups()[0] + ' ' + match.groups()[1]
        try:
            devices[ipmac] += 1
        except:
            devices[ipmac] = 1
    with open ('log2.csv', 'w', newline='') as fout:
        csvout = csv.writer(fout)
        csvout.writerow(['Mac Address', 'IP Address', 'DHCPACK Count'])
        for device, count in devices.items():
            temp = device.split()
            temp.append(count)
            csvout.writerow(temp)



if __name__ == '__main__':
    main()
