# Grade Calculator - A program that will average 3 numeric exam grades,
# return an average test score, a corresponding letter grade, and a message
# stating whether the student is passing. 

# 90+ = A, 80 - 89 =B, 70 - 79 = C 60 - 69 = D, < 59 = F

def main():
    # Get Data from user and put into an array
    exam_one = int(input('Input exam grade one: '))
    while exam_one > 100:
        exam_one = int(input('Input a valid percentage(0-100): '))
    exam_two = int(input('Input exam grade two: '))
    while exam_two > 100:
        exam_two = int(input('Input a valid percentage(0-100): '))
    exam_three = int(input('Input exam grade three: '))
    while exam_three > 100:
        exam_three = int(input('Input a valid percentage(0-100): '))
    grades = [exam_one, exam_two, exam_three]

    # Call functions to do the work and print output
    gavg = avg(grades)
    letter_grade = grade2letter(gavg)
    print_grade(grades, gavg, letter_grade)

def avg(grades):
    # Get average grade
    mysum = 0
    for grade in grades:
        mysum += grade
    return(mysum / len(grades))

def grade2letter(gavg):
    # Convert grade to letter grade
    if gavg >= 90:
        letter_grade = "A"
    elif gavg >= 80 and gavg < 90:
        letter_grade = "B"
    elif gavg >= 70 and gavg < 80:
        letter_grade = 'C'
    elif gavg >= 60 and gavg < 70:
        letter_grade = "D"
    else:
        letter_grade = "F"
    return(letter_grade)

# Print out the grade
def print_grade(grades, number_grade, letter_grade):
    print(' ')
    for grade in grades:
        print("Exam: " + str(grade))
    print("Average: " + str(number_grade))
    print("Grade: " + str(letter_grade))

    if letter_grade == "F":
        print("Student is failing.")
    else:
        print ("Student is passing.")

# Run main() if script called directly
if __name__ == "__main__":
    main()
