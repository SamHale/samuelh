#!/usr/bin/python3
#This script asks for a username and favorite color then prints 
#out 'The favorite color for <username> is <color>.'.
#
#Samuel-20210427: Initial version
#

#main routine
def main():
    username = input("Enter your username: ")
    color = input("Enter your favorite color: ")
    print("The favorite color for " + username + " is " + color + ".")

#run main if script called directly
if __name__ =="__main__":
    main()

